## Travis Busen, P1: Test a Perceptual Phenomenon


# Questions for investigation

1)  What is our independent variable?  What is our dependent variable?

2)  What is an appropriate set of hypotheses for this task?  What kind of statistical test do you expect to perform?  Justify your choices.

3) Report some descriptive statistics regarding this dataset.  Include at least one measure of central tendency and at least one measure of variability.

4)  Provide one or two visualizations that show the distribution of the sample data.  Write one or two sentences noting what you observe about the plot or plots.

5)  Now, perform the statistical test and report your results.  What is the confidence level and your critical statistical value?  Do you reject the null hypothesis or fail to reject it?  Come to a conclusion in terms of the experiment task.  Did the results match up with your expectations?

6)  Optional:  What do you think is responsible for the effects observed?  Can you think of an alternative or similar task that would result in a similar effect?  Some research about the problem will be helpful for thinking about these two questions!


# Answers

1)  The independent variable is the color of the ink.  The dependent variable is the time it takes to name the ink colors.

2)  The set of hypotheses

$H_0$:  The color of the ink matching the word displayed does not affect the time it takes to name the color of the ink

$H_A$:  The color of the ink matching the word displayed does affect the time it takes to name the color of the ink

3)  Descriptive statistics


```python
# create data frame containing the sample data

import pandas as pd
import math
# allow for inline plotting in Jupyter
%matplotlib inline

stroop_df = pd.DataFrame.from_items([('congruent',[12.079,16.791,9.564,8.63,14.669,12.238,14.692,8.987,9.401,14.48,
                                                   22.328,15.298,15.073,16.929,18.2,12.13,18.495,10.639,11.344,12.369,
                                                   12.944,14.233,19.71,16.004]),
                                     ('incongruent', [19.278,18.741,21.214,15.687,22.803,20.878,24.572,17.394,20.762,
                                                      26.282,24.524,18.644,17.51,20.33,35.255,22.158,25.139,20.429,
                                                      17.425,34.288,23.894,17.96,22.058,21.157])])
```


```python
# sample values to make sure df was made correctly
stroop_df.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>congruent</th>
      <th>incongruent</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>12.079</td>
      <td>19.278</td>
    </tr>
    <tr>
      <th>1</th>
      <td>16.791</td>
      <td>18.741</td>
    </tr>
    <tr>
      <th>2</th>
      <td>9.564</td>
      <td>21.214</td>
    </tr>
    <tr>
      <th>3</th>
      <td>8.630</td>
      <td>15.687</td>
    </tr>
    <tr>
      <th>4</th>
      <td>14.669</td>
      <td>22.803</td>
    </tr>
  </tbody>
</table>
</div>




```python
# gather descriptive statistics for each column
# use mean as central tendancy and standard deviation for measure of variability

# calculate the mean and standard deviation by using the .mean() and .std() method
# for each of the samples in the stroop_df dataframe

# mean
congruent_mean = stroop_df["congruent"].mean()
incongruent_mean = stroop_df["incongruent"].mean()
# standard deviation
congruent_sd = stroop_df["congruent"].std()
incongruent_sd = stroop_df["incongruent"].std()
```


```python
# print the variable values

print("The mean of the Congruent sample is: " + str(congruent_mean))
print("The mean of the Incongruent sample is: " + str(incongruent_mean))
print(" ")      
print("The sample standard deviation of the Congruent sample is: " + str(congruent_sd))
print("The sample standard deviation of the Incongruent sample is: " + str(incongruent_sd))
```

    The mean of the Congruent sample is: 14.051125
    The mean of the Incongruent sample is: 22.0159166667

    The sample standard deviation of the Congruent sample is: 3.55935795765
    The sample standard deviation of the Incongruent sample is: 4.79705712247


4) Three visualizations.  A histogram of each sample and  a stacked histogram with each sample plotted on
the same X and Y axis.


```python
stroop_df["congruent"].plot.hist(legend=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x10e493690>




![png](output_7_1.png)



```python
stroop_df["incongruent"].plot.hist(legend=True, color='green')
```




    <matplotlib.axes._subplots.AxesSubplot at 0x10e79a150>




![png](output_8_1.png)



```python
stroop_df.plot.hist(legend=True, stacked=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x10e92ee90>




![png](output_9_1.png)


#### Both the congruent and incongruent samples are skewed to the right.   The incongruent  sample is shifted more to the right than  the congruent sample indicating that the time to identify the ink color took longer.

5) Conduct a two tailed t-test with 95% confidence.

* Find the differences in means between the the two samples
* Find the t-critical value
* Find the Standard Error
* Find the t-statistic


```python
# find the difference in means
delta_mean = congruent_mean - incongruent_mean

# print the results
print("The difference in means is: " + str(round(delta_mean,2)))
```

    The difference in means is: -7.96


#### Find the t-critical value

The sample size is 24 which makes the degrees of freedom (df) 23.  It is a two tailed t-test at 95% confidence.  We find the value where df = 23 and p = .025.

<img src="/Users/Travis/ttable.jpeg">


```python
# find the standard deviation of the differences

# create series containing the difference in seconds for each sample between the
# congruent and incongruent values
stroop_diff_df = stroop_df["congruent"]-stroop_df["incongruent"]

# rename the series
stroop_diff_df.name ="differences"

# standard deviation of the difference in means
stroop_diff_sd = stroop_diff_df.std()

# the sample size (n)
stroop_diff_n = 24
```

$SE =  \dfrac{SD}{\sqrt{n}}$


```python
# standard error
stroop_SE = round(stroop_diff_sd / math.sqrt(stroop_diff_n),2)
```

Calculate the  t-statistic

$\dfrac{\bar{X} - \mu}{SE}$


```python
# t-statistic
stroop_tstat = round(delta_mean / stroop_SE,2)

# print the result
print("The t-statistic is : " + str(stroop_tstat))
```

    The t-statistic is : -8.05


The t-critical value was +/- 2.069 and the t-statistic was -8.05.  This is less than the critical value.  We reject the null hypothesis.  The color of the ink matching the word displayed does appear to have an effect.

6)  Calculating the  $r^2$ results in .74.  74% of the difference in means can be attributed to the color of the ink matching the word displayed.  According to the Wikipedia article on the <a href="https://en.wikipedia.org/wiki/Stroop_effect">Stroup Effect</a> the underlying issue has to do with interference.  Both the word and the color of the ink have meaning and need to be processed by the brain.  When they are the same ie <font color = "red">RED</font> it is easier to process because the answer to either question is the same (What word & what color are both red).  When there is interference ie  <font color = "red">YELLOW</font> the brain has to process both and really think about the question.

Per the documented variations of this test, another situation that I found interesting is using integers, but varying their size and asking which one is larger (in value or physical size).

$r^2 = \dfrac{t^2}{t^2 + df}$


```python
# calculate r squared
stroop_r2 = round((stroop_tstat**2)/(stroop_tstat**2 + (stroop_diff_n - 1)),2)

# print the result
print("r squared is : " + str(stroop_r2))
```

    r squared is : 0.74

Sources:

<a href="http://stackoverflow.com/questions/31609600/jupyter-ipython-notebook-not-plotting">Stackovrflow_1</a>

<a href="http://stackoverflow.com/questions/18553503/how-to-rename-a-column-of-a-pandas-core-series-timeseries-object">Stackovrflow_2</a>

<a href="http://web.ift.uib.no/Teori/KURS/WRK/TeX/symALL.html">Latex Math</a>

<a href="https://docs.python.org/2/library/math.html">Python Math Library</a>
